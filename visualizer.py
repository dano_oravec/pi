try:
    from Tkinter import *
except ImportError:
    from tkinter import *
from dart import Dart


class Visualizer:

    def __init__(self, _side, cmp):
        self.comp = cmp
        self.root = Tk()
        self.side = _side
        self.message_box_width = self.side
        self.message_box_height = self.side / 4
        self.canvas = Canvas(self.root, width=self.side, height=self.side + self.message_box_height)
        self.canvas.pack()
        self.canvas.grid(row=0, column=0, rowspan=8, columnspan=8)
        self.dart_r = 1
        self.darts = []

        # Initial drawing
        self.rect = self.canvas.create_rectangle(0, 0, self.side, self.side, fill='blue')
        self.circle = self.canvas.create_oval(0, 0, self.side, self.side, fill='red')
        self.vertical = self.canvas.create_line(self.side / 2, 0, self.side / 2, self.side)
        self.horizontal = self.canvas.create_line(0, self.side / 2, self.side, self.side / 2)
        self.message_box = self.canvas.create_rectangle(
            0, self.side, self.message_box_width, self.side + self.message_box_height, fill='gold'
        )
        self.text = self.canvas.create_text(0, 0, text='')
        self.canvas.pack()

    def add_dart(self, x, y):
        self.darts.append(Dart(x, y))

    def redraw(self, value):
        for dart in self.darts:
            self.canvas.create_oval(
                dart.x - self.dart_r, dart.y - self.dart_r, dart.x + self.dart_r, dart.y + self.dart_r,
                fill='yellow' if self.comp.is_in_circle([dart.x, dart.y]) else 'green'
            )
        self.darts = []  # Reset so we won't redraw same darts again
        self.canvas.delete(self.text)
        self.text = self.canvas.create_text(
            self.side // 2, self.side + self.message_box_height // 2,
            text=str('%.10f' % value), fill='red', font=("Serif", 40)
        )
        self.canvas.pack()
