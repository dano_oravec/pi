from visualizer import Visualizer
from comp import Comp
import time

darts = 0
circle_cnt = 0
d = 600
C = Comp(d)
V = Visualizer(d, C)


def update():
    global V
    global C
    global darts
    global circle_cnt
    at_once = 100
    for i in range(at_once):
        darts += 1
        cartesian = C.get_random_coordinates()
        if C.is_in_circle(cartesian):
            circle_cnt += 1
        V.add_dart(cartesian[0], cartesian[1])
    V.canvas.update()
    pi_approximation = (circle_cnt / darts) * 4
    V.redraw(pi_approximation)
    V.root.after(100, update)

if __name__ == '__main__':
    V.root.after(100, update)
    V.root.mainloop()
