import random


class Comp:
    def __init__(self, a):
        self.a = a  # Edge length

    def is_in_circle(self, cartesian):
        dist = (cartesian[0] - self.a / 2.0) ** 2 + (cartesian[1] - self.a / 2.0) ** 2
        return dist <= (self.a / 2) ** 2

    def get_random_coordinates(self):
        return [random.uniform(1, self.a) - 1, random.uniform(1, self.a) - 1]
        # return [random.randint(1, self.a) - 1, random.randint(1, self.a) - 1]
